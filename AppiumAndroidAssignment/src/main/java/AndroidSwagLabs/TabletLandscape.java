package androidSwagLabs;

import java.io.IOException;

import org.testng.annotations.BeforeClass;

import wrapper.ProjectWrappers;

public class TabletLandscape extends ProjectWrappers {

	@BeforeClass
	public void setValues(){
		dataSheetName 	= "Test";
		testCaseName 	= "Landscape and Portraid";
		testDescription = "To verify whether the user can use the landscape and portraid mode";
		category = "Landscape and Portraid";
		authors = "Jabrulla";

	}

	@org.testng.annotations.Test(dataProvider="fetchData")
	public void login(String deviceName, String deviceVersion) throws IOException, InterruptedException {
		launchApp(deviceName, deviceVersion);
		screenOrientation();
		screenOrientation();
	}

}
