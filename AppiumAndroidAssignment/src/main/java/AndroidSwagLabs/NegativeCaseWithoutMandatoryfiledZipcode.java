package androidSwagLabs;

import java.io.IOException;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import wrapper.ProjectWrappers;

public class NegativeCaseWithoutMandatoryFiled extends ProjectWrappers {

	@BeforeClass
	public void setValues(){
		dataSheetName 	= "Test";
		testCaseName 	= "AddtoCart";
		testDescription = "To verify whether the user can Add Products";
		category = "AddtoCart";
		authors = "Jabrulla";
	}
	
	@Test(dataProvider="fetchData")
	public void login(String deviceName, String deviceVersion,String Username,String Password,String FirstName,String LastName,String Zipcode) throws IOException, InterruptedException {

		launchApp(deviceName, deviceVersion);
		Scrolldown(1);
		enterByAccessebilityID("test-Username",Username);
		enterByAccessebilityID("test-Password",Password);
		clickByAccessebilityID("test-LOGIN");
		clickByXpath("(//android.view.ViewGroup[@content-desc='test-ADD TO CART'])[1]");
		clickByXpath("//android.view.ViewGroup[@content-desc='test-Cart']/android.view.ViewGroup/android.widget.TextView")
		Scrolldown(1);
		verifyTextByAccessebilityID("test-Price","$29.99");
		clickByAccessebilityID("test-CHECKOUT");
		enterByAccessebilityID("test-First Name",FirstName);
		enterByAccessebilityID("test-Last Name",LastName);
		clickByAccessebilityID("test-CONTINUE");
		verifyTextByXpath("//android.view.ViewGroup[@content-desc=\"test-Error message\"]/android.widget.TextView");",'Postal Code is required');
	
	
	}
}
