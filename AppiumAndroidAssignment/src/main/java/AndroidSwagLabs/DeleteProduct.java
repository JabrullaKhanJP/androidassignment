package androidSwagLabs;

import java.io.IOException;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import wrapper.ProjectWrappers;

public class DeleteProduct extends ProjectWrappers {

	@BeforeClass
	public void setValues(){
		dataSheetName 	= "Test";
		testCaseName 	= "AddtoCart";
		testDescription = "To verify whether the user can delete the product from add to cart";
		category = "AddtoCart";
		authors = "Jabrulla";
	}
	
	@Test(dataProvider="fetchData")
	public void login(String deviceName, String deviceVersion,String Username,String Password,String FirstName,String LastName,String Zipcode) throws IOException, InterruptedException {

		launchApp(deviceName, deviceVersion);
		Scrolldown(1);
		enterByAccessebilityID("test-Username",Username);
		enterByAccessebilityID("test-Password",Password);
		clickByAccessebilityID("test-LOGIN");
		clickByXpath("(//android.view.ViewGroup[@content-desc='test-ADD TO CART'])[1]");
		clickByXpath("//android.view.ViewGroup[@content-desc='test-Cart']/android.view.ViewGroup/android.widget.TextView")
		//To verify whether the QTY Before remove the product
		verifyTextByXpath("//android.view.ViewGroup[@content-desc='test-Amount']/android.widget.TextView",'1');
		//Swipe from right to left and delete the product
		int y2=driver.findElement(MobileBy.xpath("//android.view.ViewGroup[@content-desc='test-Delete'android.view.ViewGroup")).getLocation().getY();
		Dimension size2 = driver.manage().window().getSize();
		int xfrom2 = (int) (size2.width*0.30);
		int xto2 = (int) (size2.width*0.50);
		System.out.println(y2);
		SwipeHorizontalRightToLeft(xto2, y2, xfrom2);
		clickByXpath("//android.view.ViewGroup[@content-desc='test-Delete'android.view.ViewGroup");
		//To verify whether the QTY after remove the product
		verifyTextByXpath("//hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[3]/android.widget.TextView[1]",'0');
		
		
		
	
	}
}
