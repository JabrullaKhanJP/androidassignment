package androidSwagLabs;

import java.io.IOException;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import wrapper.ProjectWrappers;

public class NegativeCaseInvalidCredentials extends ProjectWrappers {

	@BeforeClass
	public void setValues(){
		dataSheetName 	= "Test";
		testCaseName 	= "AddtoCart";
		testDescription = "To verify whether the user can test NegativeCaseInvalidCredentials";
		category = "AddtoCart";
		authors = "Jabrulla";
	}
	
	@Test(dataProvider="fetchData")
	public void login(String deviceName, String deviceVersion,String Username) throws IOException, InterruptedException {

		launchApp(deviceName, deviceVersion);
		Scrolldown(1);
		enterByAccessebilityID("test-Username",hello);
		enterByAccessebilityID("test-Password",hello);
		clickByAccessebilityID("test-LOGIN");
		//enter invalid credentials and verify exception
		verifyTextByXpath("//android.view.ViewGroup[@content-desc=\"test-Error message\"]/android.widget.TextView",'Username and password do not match any user in this service.');
		enterByAccessebilityID("test-Username",Username);
		enterByAccessebilityID("test-Password",Password);
		clickByAccessebilityID("test-LOGIN");
		//Verify Products filed in home screen
		verifyTextByXpath("//android.view.ViewGroup[@content-desc=\"test-Cart drop zone\"]/android.view.ViewGroup/android.widget.TextView");",'PRODUCTS');
	
	}
}
