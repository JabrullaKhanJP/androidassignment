package wrapper;

import static io.appium.java_client.touch.WaitOptions.waitOptions;
import static io.appium.java_client.touch.offset.ElementOption.element;
import static io.appium.java_client.touch.offset.PointOption.point;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.time.Duration;
import java.util.Properties;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.ScreenOrientation;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.google.common.base.Function;
import com.relevantcodes.extentreports.ExtentTest;

import io.appium.java_client.MobileBy;
import io.appium.java_client.MultiTouchAction;
import io.appium.java_client.TouchAction;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.Connection;
import utils.Reporter;


public class WrapperMethods  extends Reporter implements Wrappers{

	public AndroidDriver<?> driver;
	protected static Properties prop;

	String apppackage= "com.swaglabsmobileapp"";
	String appactivity="MainActivity";
	private int width;
	public WrapperMethods(AndroidDriver<?> driver, ExtentTest test) {
		this.driver = driver;
		this.test=test;
	}

	public boolean launchApp(String deviceName,String version ){
		try {
			DesiredCapabilities dc = new DesiredCapabilities();
			dc.setCapability("appPackage", apppackage);
			dc.setCapability("appActivity", appactivity);
			dc.setCapability("deviceName", deviceName);
			//dc.setCapability("mobile", deviceName);
			dc.setVersion(version);
			dc.setCapability("noReset",true);
			dc.setCapability("nativeWebScreenshot", "true");
			driver = new AndroidDriver<WebElement>(new URL("http://0.0.0.0:4723/wd/hub"), dc);
			reportStep("The Application package:" + apppackage + " launched successfully", "PASS");
		} catch (MalformedURLException e) {
			e.printStackTrace();
			reportStep("The Application package:" + apppackage + " could not be launched", "FAIL");
		}
		return true;	
	}

	public boolean screenOrientation(){
		try {
			ScreenOrientation orientation= driver.getOrientation();
			System.out.println(orientation.value());

			if(orientation.value().contains("LANDSCAPE"))
			{
				driver.rotate(ScreenOrientation.PORTRAIT);
				ScreenOrientation or  = driver.getOrientation();
				reportStep("The Screen is in "+or+" ", "PASS");
			}
			else
			{
				driver.rotate(ScreenOrientation.LANDSCAPE);				
				ScreenOrientation or =driver.getOrientation();
				reportStep("The Screen is in "+or+" ", "PASS");
			}
		} catch (Exception e) {
			e.printStackTrace();
			reportStep("App closed for unknown reason", "FAIL");
		}
		return true;
	}

		public boolean hideKeyBoard(){
		try {
			driver.hideKeyboard();
			reportStep("The Keyboard is hidden successfully", "PASS");

		} catch (Exception e) {
			e.printStackTrace();
			reportStep("The Keyboard is still available", "FAIL");
		}
		return true;
	}
	public boolean getKeyboard(String data){
		try {
			driver.getKeyboard().sendKeys(data);
			reportStep("The data: " + data + " entered successfully in field", "PASS");

		} catch (NoSuchElementException e) {
			reportStep("The data: " + data + " could not be entered in the field", "FAIL");
		} catch (Exception e) {
			reportStep(e.getLocalizedMessage() + ": " + data + " in the field ", "FAIL");
		}
		return true;
	}


	public void touchActionTapBycoordinates(int x , int y ) throws InterruptedException, IOException
	{
		try {
			new TouchAction<>(driver)
			.press(point(x, y)).release().perform();
			reportStep("The element is clicked successfully with "+x+ " " +y , "PASS");
		} catch (Exception e) {
			reportStep("The element is not clicked successfully with "+x+ " " +y , "FAIL");
		}
	}
	public void verifyTextByText(String textFromSource, String textFromDestnation) {
		try {
			String sText = textFromSource;
			String text = textFromDestnation;
			if (sText.equalsIgnoreCase(text)) {
				reportStep("The text: " + sText + " matches with the value :" + text, "PASS");

			} else {
				reportStep("The text: " + sText + " did not match with the value :" + text, "FAIL");

			}
		} catch (Exception e) {
			reportStep("Unknown exception occured while verifying the text", "FAIL");
		}

	}

	public boolean verifyAndInstallApp(String appPackage, String appPath) {
		boolean bInstallSuccess = false;
		try {
			if (driver.isAppInstalled(appPackage))
				driver.removeApp(appPackage);
			driver.installApp(appPath);
			reportStep("The Application:" + appPackage + " installed successfully", "PASS");
			bInstallSuccess = true;
		} catch (Exception e) {
			reportStep("The Application:" + appPackage + " could not be installed", "FAIL");
			// TODO: handle exception
		}
		return bInstallSuccess;
	}

	public void sleep(int mSec){
		try {
			Thread.sleep(mSec);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	
	public void printContext(){
		try {
			Set<String> contexts = driver.getContextHandles();
			for (String string : contexts) {
				System.out.println(string);
			}
		} catch (Exception e) {
			e.printStackTrace();
			reportStep("The Context could not be found", "FAIL");

		}
	}

	public boolean switchview(){
		try {
			Set<String> contexts = driver.getContextHandles();
			for (String contextName : contexts) {
				if (contextName.contains("NATIVE"))
					driver.context(contextName);
			}

		} catch (Exception e) {
			e.printStackTrace();
			reportStep("The Context could not be switched", "FAIL");
		}
		return true;
	}
	public boolean clickByID(String id){
		this.waitForLoad();
		try {
			//WebDriverWait wait = new WebDriverWait(driver, 30);
			//wait.until(ExpectedConditions.presenceOfElementLocated(By.id(id)));
			driver.findElementById(id).click();
			reportStep("The element with id: "+id+" is clicked.", "PASS");
		} catch (Exception e) {
			e.printStackTrace();
			reportStep("The element with id: "+id+" could not be clicked.", "FAIL");
		}
		return true;
	}

	public boolean clickByAccessebilityID(String id){
		try {
			/*WebDriverWait wait = new WebDriverWait(driver, 30);
			wait.until(ExpectedConditions.presenceOfElementLocated(ByAccessibilityId(ID));*/
			driver.findElementByAccessibilityId(id).click();
			reportStep("The element with Accessibility id: "+id+" is clicked.", "PASS");
		} catch (Exception e) {
			e.printStackTrace();
			reportStep("The element with Accessibility id: "+id+" could not be clicked.", "FAIL");
		}
		return true;
	}

	
	public boolean enterByAccessebilityID(String id){
		try {
			/*WebDriverWait wait = new WebDriverWait(driver, 30);
			wait.until(ExpectedConditions.presenceOfElementLocated(ByAccessibilityId(ID));*/
			driver.findElementByAccessibilityId(id).click();
			reportStep("The element with Accessibility id: "+id+" is clicked.", "PASS");
		} catch (Exception e) {
			e.printStackTrace();
			reportStep("The element with Accessibility id: "+id+" could not be clicked.", "FAIL");
		}
		return true;
	}

	
	/* (non-Javadoc)
	 * @see wrapper.Wrappers#clickByXpath(java.lang.String)
l	 */
	public boolean clickByXpath(String xpath){
		this.waitForLoad();
		try {
			/*WebDriverWait wait = new WebDriverWait(driver, 30);
			wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(xpath)));*/
			driver.findElementByXPath(xpath).click();
			reportStep("The element with Xpath: "+xpath+" is clicked.", "PASS");

		} catch (Exception e) {
			e.printStackTrace();
			reportStep("The element with Xpath: "+xpath+" is clicked.", "PASS");
		}
		return true;
	}

	/* (non-Javadoc)
	 * @see wrapper.Wrappers#verifyContentDescIsDisplayed(java.lang.String)
	 */
	public boolean verifyContentDescIsDisplayed(String xpath){
		boolean bReturn = false;
		WebDriverWait wait = new WebDriverWait(driver, 30);
		wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(xpath)));
		if(driver.findElementByXPath(xpath).isDisplayed()){
			bReturn = true;
			reportStep("The element with Xpath: "+xpath+" is displayed.", "PASS");
		}else
		{
			reportStep("The element with Xpath: "+xpath+" is not displayed.", "FAIL");
			bReturn = false;
		}
		return bReturn;

	}

	public boolean clickByLinkText(String LinkText){
		boolean bReturn = false;
		try {
			WebDriverWait wait = new WebDriverWait(driver, 30);
			wait.until(ExpectedConditions.presenceOfElementLocated(By.linkText(LinkText)));
			driver.findElementByLinkText(LinkText).click();
			reportStep("The element with LinkText: "+LinkText+" is clicked.", "PASS");
			bReturn = true;
		} catch (Exception e) {
			e.printStackTrace();
			reportStep("The element with LinkText: "+LinkText+" could not be clicked.", "FAIL");
			bReturn = false;
		}
		return bReturn;
	}

	public void enterTextByID(String id,String data){
		try {
			WebDriverWait wait = new WebDriverWait(driver, 30);
			wait.until(ExpectedConditions.presenceOfElementLocated(By.id(id)));
			driver.findElementById(id).clear();
			driver.findElementById(id).sendKeys(data);
			reportStep("The data: "+data+" entered successfully in the field with Id:"+id, "PASS");
		} catch (NoSuchElementException e) {
			reportStep("The data: "+data+" could not be entered in the field with Id:"+id, "FAIL");
		} catch (Exception e) {
			reportStep("Unknown exception occured while entering "+data+" in the field with Id:"+id, "FAIL");
		}

	}


	public WrapperMethods() {
		Properties prop = new Properties();
		try {
			prop.load(new FileInputStream(new File("./config.properties")));
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void loadObjects() throws FileNotFoundException, IOException{
		prop = new Properties();
		prop.load(new FileInputStream(new File("./object.properties")));

	}

	public void pressEnter(){
		try {
			driver.pressKeyCode(66);
			reportStep("Enter button in the keyboard pressed successfully", "PASS");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			reportStep("Enter button in the keyboard could not be pressed successfully", "FAIL");
		}
	}


	public void enterTextByXpath(String xpath,String data){
		try {
			WebDriverWait wait = new WebDriverWait(driver, 30);
			wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(xpath)));
			driver.findElementByXPath(xpath).clear();
			driver.findElementByXPath(xpath).sendKeys(data);
			reportStep("The data: "+data+" entered successfully in the field with Xpath:"+xpath, "PASS");

		} catch (NoSuchElementException e) {
			reportStep("The data: "+data+" could not be entered in the field with Xpath:"+xpath, "FAIL");
		} catch (Exception e) {
			reportStep("Unknown exception occured while entering "+data+" in the field with Xpath:"+xpath, "FAIL");
		}

	}
	public void enterByXpath(String xpathValue, String data) {

		try {
			driver.findElement(By.xpath(xpathValue)).clear();
			driver.findElement(By.xpath(xpathValue)).sendKeys(data);
			reportStep("The data: " + data + " entered successfully in field :" + xpathValue, "PASS");

		} catch (NoSuchElementException e) {
			reportStep("The data: " + data + " could not be entered in the field :" + xpathValue, "FAIL");
		} catch (Exception e) {
			reportStep(e.getLocalizedMessage() + ": " + data + " in the field :" + xpathValue, "FAIL");
		}

	}


		public void enterById(String idValue, String data) {
		waitForLoad();
		try {
			driver.findElement(By.id(idValue)).clear();
			driver.findElement(By.id(idValue)).sendKeys(data);
			reportStep("The data: " + data + " entered successfully in field :" + idValue, "PASS");
		} catch (NoSuchElementException e) {
			reportStep("The data: " + data + " could not be entered in the field :" + idValue, "FAIL");
		} catch (Exception e) {
			reportStep("Unknown exception occured while entering " + data + " in the field :" + idValue, "FAIL");
		}
	}
	public long takeSnap(){
		long number = (long) Math.floor(Math.random() * 900000000L) + 10000000L; 
		try {
			FileUtils.copyFile(driver.getScreenshotAs(OutputType.FILE) , new File("./reports/images/"+number+".jpg"));
		} catch (WebDriverException e) {
			reportStep("The browser has been closed.", "FAIL");
		} catch (IOException e) {
			reportStep("The snapshot could not be taken", "WARN");
		}
		return number;
	}

	public boolean verifyTextByID(String id,String data){
		boolean bReturn = false;
		try {
			WebDriverWait wait = new WebDriverWait(driver, 30);
			wait.until(ExpectedConditions.presenceOfElementLocated(By.id(id)));
			String name = driver.findElementById(id).getText();
			if(name.contains(data)){
				bReturn = true;
				reportStep("The text: "+name+" matches with the value :"+data, "PASS");
			}else{
				bReturn = false;
				reportStep("The text: "+name+" did not match with the value :"+data, "FAIL");
			}
		} catch (Exception e) {
			e.printStackTrace();
			reportStep("Unknown exception occured while verifying the title", "FAIL");
		}
		return bReturn;
	}

	public boolean verifyTextByXpath(String xpath,String data){
		boolean bReturn = false;
		try {
			//WebDriverWait wait = new WebDriverWait(driver, 30);
			//wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(xpath)));
			String name = driver.findElementByXPath(xpath).getText();
			if(name.contains(data)){
				bReturn = true;
				reportStep("The text: "+name+" matches with the value :"+data, "PASS");
			}else
			{
				bReturn = false;
				reportStep("The text: "+name+" did not match with the value :"+data, "FAIL");
			}
		} catch (Exception e) {
			e.printStackTrace();
			reportStep("Unknown exception occured while verifying the title", "FAIL");
		}
		return bReturn;
	}
	public void verifyTextByAccessebilityID(String textFromSource, String textFromDestnation) {
		try {
			String sText = textFromSource;
			String text = textFromDestnation;
			if (sText.equalsIgnoreCase(text)) {
				reportStep("The text: " + sText + " matches with the value :" + text, "PASS");

			} else {
				reportStep("The text: " + sText + " did not match with the value :" + text, "FAIL");

			}
		} catch (Exception e) {
			reportStep("Unknown exception occured while verifying the text", "FAIL");
		}
	}

	public boolean backButton(){
		try {
			driver.navigate().back();
			reportStep("The Application screen is moved back", "PASS");
		} catch (Exception e) {
			e.printStackTrace();
			reportStep("The Application screen could not be moved back", "PASS");
		}
		return true;
	}
	public void enterTextByXpathUsingActions(String xpath,String data){
		try {
			WebDriverWait wait = new WebDriverWait(driver, 30);
			wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(xpath)));
			Actions actions = new Actions(driver);
			actions.moveToElement(driver.findElement(By.xpath(xpath)));
			actions.click();
			actions.sendKeys(Keys.chord(Keys.CONTROL, "a"));
			actions.sendKeys(data);
			actions.build().perform();
			reportStep("The data: "+data+" entered successfully in the field with Xpath:"+xpath, "PASS");

		} catch (NoSuchElementException e) {
			reportStep("The data: "+data+" could not be entered in the field with Xpath:"+xpath, "FAIL");
		} catch (Exception e) {
			reportStep("Unknown exception occured while entering "+data+" in the field with Xpath:"+xpath, "FAIL");
		}

	}

	public boolean closeApp(){
		try {
			driver.closeApp();
			reportStep("The Appication is closed successfully", "PASS");

		} catch (Exception e) {
			e.printStackTrace();
			reportStep("The Appication could not be closed", "FAIL");
		}
		return true;
	}


	public void Load() {

		try {
			while (isAsyncLoading()) {
				System.out.println("Wating for load...");
				try {
					Thread.sleep(3000);
				} catch (InterruptedException e1) {
					e1.printStackTrace();
				}
			}

		} catch (TimeoutException tex) {
			System.out.println("Time out reached!");
		} finally {
			try {
				Thread.sleep(2000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}

	public boolean switchWebview(){
		try {		
			Set<String> contextNames = driver.getContextHandles();

			for (String contextName : contextNames) {

				if (contextName.contains("WEBVIEW"))
					driver.context(contextName);				

			}
		} catch (Exception e) {
			e.printStackTrace();
			reportStep("The Webview couldnot be found", "FAIL");
		}
		return true;
	}

	public boolean launchApp(String appPackage, String appActivity, String deviceName) {
		// TODO Auto-generated method stub
		return false;
	}

	public void clickXpathUsingActions(String xpath){
		try {
			WebDriverWait wait = new WebDriverWait(driver, 30);
			wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(xpath)));
			Actions actions = new Actions(driver);
			actions.moveToElement(driver.findElement(By.xpath(xpath)));
			actions.click();

			actions.build().perform();
			reportStep("The element is clicked successfully with "+xpath, "PASS");

		} catch (NoSuchElementException e) {
			reportStep("The element could not be clicked with Xpath:"+xpath, "FAIL");
		} catch (Exception e) {
			reportStep("Unknown exception occured while clicking with Xpath:"+xpath, "FAIL");
		}

	}

	@Override
	public boolean launchActivity(String appPackage, String appActivity) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean scrollUsingDesc(String text) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean scrollUpinApp() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean scrollHalfinApp() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean zoomInApp() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean zoomUsingElement(String xpath) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean pinchUsingElement(String xpath) {
		// TODO Auto-generated method stub
		return false;
	}


	public void SwipeHorizontalLeftToright(int xFrom ,int yFrom, int xto ) throws InterruptedException, IOException
	{
		try {
			new TouchAction<>(driver)
			.press(point(xFrom, yFrom))
			.waitAction(waitOptions(Duration.ofMillis(5000)))
			.moveTo(point(xto, yFrom))
			.release()
			.perform();		reportStep("Swiped successfully", "PASS");
		} catch (Exception e) {
			reportStep("Not Swiped successfully" , "FAIL");
		}

	}

	public void SwipeHorizontalRightToLeft(int xFrom ,int yFrom, int xto ) throws InterruptedException, IOException
	{
		try {
			new TouchAction<>(driver)
			.press(point(xFrom, yFrom))
			.waitAction(waitOptions(Duration.ofMillis(5000)))
			.moveTo(point(xto, yFrom))
			.release()
			.perform();		reportStep("Swiped successfully", "PASS");
		} catch (Exception e) {
			reportStep("Not Swiped successfully" , "FAIL");
		}
	}

	public void ScrollDown( ) throws InterruptedException, IOException
	{
		try {

			Dimension size = driver.manage().window().getSize();

			int x= (int) (size.width*0.50);
			int yfrom = (int) (size.height*0.20);
			int yto = (int) (size.height*0.80);

			new TouchAction<>(driver)
			.press(point(x, yfrom))
			.waitAction(waitOptions(Duration.ofMillis(5000)))
			.moveTo(point(x, yto));
			reportStep("ScrollDown successfully", "PASS");
		} catch (Exception e) {
			reportStep("Not ScrollDown successfully" , "FAIL");
		}
	}

	private void release() {
		// TODO Auto-generated method stub

	}
	private void perform() {
		// TODO Auto-generated method stub

	}

	public void Scrolldown(int ScrollTimes ) throws InterruptedException, IOException
	{
		try {
			for(int i = 1 ;i<=ScrollTimes;i++) {
				Dimension size = driver.manage().window().getSize();
				int x= (int) (size.width*0.50);
				int yfrom = (int) (size.height*0.20);
				int yto = (int) (size.height*0.60);
				new TouchAction<>(driver)
				.press(point(x, yto))
				.waitAction(waitOptions(Duration.ofMillis(5000)))
				.moveTo(point(x, yfrom))
				.release()
				.perform(); 
				reportStep("scrolldown successfully", "PASS");
			}
		} catch (Exception e) {
			reportStep("Not scrolldown successfully" , "FAIL");
		}
	}

	public void ScrollUp(int ScrollTimes ) throws InterruptedException, IOException
	{
		try {
			for(int i = 1 ;i<=ScrollTimes;i++) {
				Dimension size = driver.manage().window().getSize();
				int x= (int) (size.width*0.50);
				int yfrom = (int) (size.height*0.25);
				int yto = (int) (size.height*0.90);
				new TouchAction<>(driver)
				.press(point(x, yfrom))
				.waitAction(waitOptions(Duration.ofMillis(5000)))
				.moveTo(point(x, yto))
				.release()
				.perform(); reportStep("scrollup successfully", "PASS");
			}
		} catch (Exception e) {
			reportStep("Not scrollup successfully" , "FAIL");
		}

	}

	private void swipe(int x, int starty, int x2, int endy, int i) {
		// TODO Auto-generated method stub

	}
	
	
	public void enterByAccessebilityID(String id,String data){
		this.waitForLoad();
		Load();
		try {
			//WebDriverWait wait = new WebDriverWait(driver, 30);
			//wait.until(ExpectedConditions.presenceOfElementLocated(By.id(id)));
			driver.findElementById(id).clear();
			driver.findElementById(id).sendKeys(data);
			reportStep("The data: "+data+" entered successfully in the field with Id:"+id, "PASS");
		} catch (NoSuchElementException e) {
			reportStep("The data: "+data+" could not be entered in the field with Id:"+id, "FAIL");
		} catch (Exception e) {
			reportStep("Unknown exception occured while entering "+data+" in the field with Id:"+id, "FAIL");
		}

	}

	public void launchBrowser(String browser) {
		launchBrowser(browser, false);
	}

	private void launchBrowser(String browser,boolean b) {
		// TODO Auto-generated method stub

	}

	//this method will use for launch the browser
	public boolean launchBrowser(String deviceName,String Version,String browser,String platformName ){
		try {
			DesiredCapabilities dc = new DesiredCapabilities();
			dc.setBrowserName(browser);
			dc.setCapability("platformName", platformName);
			//dc.setCapability("DeviceName", "520391ba646f34ed");
			dc.setCapability("deviceName", deviceName);
			dc.setCapability("browserName", browser);
			dc.setVersion(Version);
			dc.setCapability("noReset",true);
			dc.setCapability("nativeWebScreenshot", "true");
			//System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
			driver = new AndroidDriver<WebElement>(new URL("http://0.0.0.0:4723/wd/hub"), dc);
			driver.get(" http://172.17.0.145:8280/tensadmin/login");
			reportStep("The browser:" + browser + " launched successfully", "PASS");
		} catch (MalformedURLException e) {
			e.printStackTrace();
			reportStep("The browser:" + browser + " could not be launched", "FAIL");
		}
		return true;	
	}


	private boolean isAsyncLoading() {
		// TODO Auto-generated method stub
		return false;
	}
	@Override
	public boolean scrollsearchElement() {
		// TODO Auto-generated method stub
		return false;
	}

	
	@Override
	public void verifyTextByAccessebilityID() {
		// TODO Auto-generated method stub

	}


	@Override
	public void clickByAccessebilityID() {
		// TODO Auto-generated method stub

	}


	@Override
	public void enterByAccessebilityID() {
		// TODO Auto-generated method stub

	}

}

