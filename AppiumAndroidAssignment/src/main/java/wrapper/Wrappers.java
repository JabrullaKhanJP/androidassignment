package wrapper;

import java.io.FileNotFoundException;
import java.io.IOException;

public interface Wrappers {

	void loadObjects() throws FileNotFoundException, IOException;

	// this method will help launch the activity
	boolean launchActivity(String appPackage, String appActivity);
	
	boolean launchApp(String appPackage, String appActivity, String deviceName);

	boolean verifyAndInstallApp(String appPackage, String appPath);
	// this method will help launch the PrintContext
	void printContext();
	// this method will help launch the SwitchView
	boolean switchview();
	// this method will help clickbyId
	boolean clickByID(String id);
//this method will help clickbyAccebilityId
	boolean clickByAccessebilityID(String id);
	//this method will help clickbyXpath
	boolean clickByXpath(String xpath);
	//this method will help Toverify the ContentDesc
	boolean verifyContentDescIsDisplayed(String xpath);
//this method will help the clickByLinkText
	boolean clickByLinkText(String LinkText);
//this Method will helpEnterByID
	void enterTextByID(String id, String data);
//this Method will help EnterButton.
	void pressEnter();
//this method will hlep enterByXpath
	void enterTextByXpath(String xpath, String data);
//this method will help VerifytxtById
	boolean verifyTextByID(String id, String data);
	// this method will Scrolldown the screen
	boolean Scrolldown();
	// this method will Scrollup the screen
	boolean ScrollUp();
		// this method will use for wait the loading 
	public void waitForLoad();
	// this method will verify the AccessebilityID
	public void verifyTextByAccessebilityID();
	//// this method will click the AccessebilityID
	public void clickByAccessebilityID();
	// this method will be use for enter the accessibility id
	public void enterByAccessebilityID();
		
	// this method will help verifyAttributeTextByXPath
	
	boolean verifyAttributeTextByXPath(String xpath, String text);
	// this method will help enterTextByXpathUsingActions
	void enterTextByXpathUsingActions(String id, String data);
	
	//This method will close the app
	boolean closeApp();

	


}